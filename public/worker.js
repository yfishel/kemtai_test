const FRAMES_COUNT = 5;
const imageSet = [];
const savedSet = [];

function addItem(item){
  if (imageSet.length === FRAMES_COUNT) {
    imageSet.shift();
  }
  imageSet.push(item);
  console.log( `Image set`, imageSet);
}

function storeItems(){
  //savedSet.push(imageSet);
  console.log( `Save images set`, imageSet);
  //console.log( `Save images set`, getImages(), getItems());
}

function getImages(){
  return imageSet;
}

function getItems(){
  return savedSet;
}

self.onmessage = (event) => {

  // store the data from the event object to a variable.
  const data = event.data;

  // Print the message to the browser's console to verify the working of the web worker.
  //console.log( `A message has been received from the main thread of type '${data.type}', with an user-generated message: ${data.msg}`);

  switch(event.data.type){
    case "imagedata":
      addItem(data.msg);
      break;
    case "stopflag":
      console.log( `Save images set`, imageSet);
      break;
    case "getdata":
      const response = {
        type: 'saveddata',
        msg: getItems()
      };
      postMessage(response);
      break;
  }


}

