const FRAMES_COUNT = 20;
const imageSet = [];
const savedSet = [];

let connected = false;

self.addEventListener("connect", e => {
  
  e.source.addEventListener("message", ev => {

    const data = ev.data;

    if(data.type === 'imagedata'){
      if (imageSet.length === FRAMES_COUNT) {
        imageSet.shift();
      }
      imageSet.push(data.msg);
      //e.source.postMessage(imageSet);
    }

    if(data.type === 'stopflag'){
      savedSet.push([...imageSet]);
      //e.source.postMessage(savedSet);
    }

    if(data.type === 'getdata'){
      const response = {
        type: 'saveddata',
        msg: savedSet
      };
      e.source.postMessage(response);
      //savedSet.splice(0, savedSet.length);
    }

    
  }, false);
  
  e.source.start();
  
}, false);