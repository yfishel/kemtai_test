import React, {FC, useCallback, useEffect, useState} from 'react';
import "./style.scss";

interface Props {
  onSuccess: () => void;
}

const CameraStartLayer: FC<Props> = ({ onSuccess }) => {

  const fakeTime = 6;

  const [counter, setCounter] = useState(0);
  const [countdown, setCountdown] = useState('');
  const [intervalId, setIntervalId] = useState(0);
  const [success, setSuccess] = useState(false);
  const [out, setOut] = useState(false);

  const handleSuccess = useCallback(()=>{
    onSuccess()
    clearInterval(intervalId)
  }, [intervalId, onSuccess])

  useEffect(()=>{

    const newIntervalId = window.setInterval(()=> setCounter(prevCount => prevCount+1), 1000);
    setIntervalId(newIntervalId);

  }, [])

  useEffect(()=>{
    if(counter === fakeTime-1){
      setSuccess(true)
    }
    if(counter >= fakeTime){
      if(counter === fakeTime){
        setCountdown('3')
      } else if(counter === fakeTime+1){
        setCountdown('2')
      } else if(counter === fakeTime+2){
        setCountdown('1')
      } else if(counter === fakeTime+3){
        setCountdown('GO!')
        setOut(true)
      } else {
        setCountdown('')
        handleSuccess()
      }
    }
  }, [counter, handleSuccess])

  return (
    <div className="section-start-layer">
      {countdown
        ? <div className={`countdown ${out ? 'out' : ''}`}>{ countdown }</div>
        : <>
          <img src="/media/bounce.svg" className={success ? 'success' : ''} alt="bounce"/>
          <div className="help-text">Please take the position corresponding to the picture</div>
          </>
      }
    </div>
  );
};

export default CameraStartLayer;