import React, { FC } from 'react';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import "./styles.scss";

interface Props {
  percents: number
}

const CircularProgress: FC<Props> = ({percents}) => {
  return (
    <div className="timer-progress-bar">
      <CircularProgressbar
        value={percents}
        strokeWidth={50}
        styles={buildStyles({
          strokeLinecap: "butt",
          pathColor: 'rgba(255, 255, 255, .8)',
          trailColor: '#222229',
          backgroundColor: '#ebebeb'
        })} />
    </div>
  );
};

export default CircularProgress;