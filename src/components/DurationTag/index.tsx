import React, { FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock } from '@fortawesome/free-regular-svg-icons';

import "./style.scss"

interface Props {
  duration: number;
}

const DurationTag:FC<Props> = ({duration}) => {
  return (
    <div className="duration-tag">
      <FontAwesomeIcon icon={faClock} />
      {duration} min
    </div>
  );
};

export default DurationTag;