import React, {FC} from 'react';
import {IWorkout} from "../../models/IWorkout";
import Header from "../Header";
import PageTitle from "../PageTitle";
import Footer from "../Footer";
import WorkoutsGrid from "../WorkoutsGrid";

interface Props {
  workouts: IWorkout[]
}

const MainView:FC<Props> = ({workouts}) => {
  return (
    <>
    <Header />
    <PageTitle text="Choose a short workout" />
    <section className="content">
      <WorkoutsGrid workouts={workouts} />
    </section>
    <Footer />
    </>
  );
};

export default MainView;