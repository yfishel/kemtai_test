import React, { useEffect, useState} from 'react';
import Webcam from "react-webcam";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {updateScoreBar} from "../../store/reducers/WorkoutsSlice";
import CameraStartLayer from "../CameraStartLayer";
import CircularProgress from "../CircularProgress";
import ScoreLabel from "../ScoreLabel";
import ScoreBar from "../ScoreBar";
import ThanksView from "../ThanksView";

import "./style.scss";

const CameraView = () => {

  const dispatch = useAppDispatch()
  const {workouts, selected, selectedReps} = useAppSelector(state => state.workoutReducer)

  const [intervalId, setIntervalId] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [percents, setPercents] = useState(0);
  const [score, setScore] = useState(0);
  const [startLayer, setStartLayer] = useState(false);
  const [permissionsLayer, setPermissionsLayer] = useState(true);
  const [thanks, setThanks] = useState(false);

  const workoutStart = ()=>{
    setStartLayer(false)
    setPermissionsLayer(false)

    const newIntervalId = window.setInterval(()=> setSeconds(prevSec => prevSec + 250/1000), 250)
    setIntervalId(newIntervalId)
  }

  const getSecondsDuration = ()=>{
    const duration = workouts.find((i)=> i.id === selected)?.duration
    return duration ? duration*60 : 60
  }

  const handleStartSuccess = ()=>{
    workoutStart();
  }

  const secondsDuration = getSecondsDuration()

  useEffect(()=>{

    if(seconds >= secondsDuration){
      clearInterval(intervalId)
      setThanks(true)
    } else {
      setPercents( seconds/secondsDuration*100 )
      if(selectedReps){
        const index = selectedReps.TimeOfMove.findIndex(elem => elem === seconds)
        if (index !== undefined && index > -1) {
          const score = selectedReps.ScoreOfMove[index] || 0
          setScore(score)
          dispatch(updateScoreBar(index))
        }
      }
    }

  }, [seconds, secondsDuration, intervalId, selectedReps])


  return (
    <section className="section-camera">

      {startLayer
        ? <CameraStartLayer onSuccess={handleStartSuccess} />
        : permissionsLayer
          ? <div />
          : <>
            <ScoreLabel score={score} />
            <CircularProgress percents={percents} />
            <ScoreBar />
            </>
      }

      {thanks
        ? <ThanksView />
        : <Webcam audio={false} onUserMedia={()=> setStartLayer(true) } videoConstraints={{facingMode:"user"}} />
      }

    </section>
  );
};

export default CameraView;