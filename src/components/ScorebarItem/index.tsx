import React, { FC, memo } from 'react';

interface Props {
  value: number;
}

const ScorebarItem: FC<Props> = ({value}) => {

  const height = value > -1 ? value/2+15 : 15;
  const background = value > -1 ? `hsl(${value/100*100}deg 100% 50%)` : '#ebebeb';

  return (
    <div style={{height:`${height}px`, background:background}} />
  );
};

export default memo(ScorebarItem);