import React, { FC } from 'react';
import "./style.scss"

interface Props {
  score: number;
}

const ScoreLabel: FC<Props> = ({score}) => {
  return (
    <div className="score-label"><div>{score}</div></div>
  );
};

export default ScoreLabel;