import React, { FC } from 'react';
import {useAppDispatch} from "../../hooks/redux";
import {selectWorkout} from "../../store/reducers/WorkoutsSlice";
import DurationTag from "../DurationTag";

import "./style.scss"

interface Props {
  id: string;
  title: string;
  image: string;
  duration: number;
}

const WorkoutCard:FC<Props> = ({id,title, image, duration}) => {

  const dispatch = useAppDispatch()

  return (
    <div onClick={(e) => dispatch( selectWorkout(id) ) } className="workout-card">
      <DurationTag duration={duration} />
      <h2 className="workout-card-title">{title}</h2>
      <img src={`/media/${image}.png`} srcSet={`/media/${image}@2x.png 2x, /media/${image}@3x.png 3x`} className="workout-card-image" alt={title} />
    </div>
  );
};

export default WorkoutCard;