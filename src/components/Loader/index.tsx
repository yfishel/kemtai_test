import React from 'react';

import './style.scss';

const Loader = () => {
  return (
    <div className="loader-wrapper">
      <div className="circle-border">
        <div className="circle-core" />
      </div>
    </div>
  );
};

export default Loader;