import React from 'react';
import {useAppSelector} from "../../hooks/redux";
import ScorebarItem from "../ScorebarItem"

import "./style.scss"

const ScoreBar = () => {

  const {scorebar} = useAppSelector(state => state.workoutReducer)

  return (
    <div className="bottom-score-bar">
      <div className="score-items-wrapper">
        { scorebar.map((score, idx) => <ScorebarItem key={idx} value={score} /> ) }
      </div>
    </div>
  );
};

export default ScoreBar;