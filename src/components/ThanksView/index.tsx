import React from 'react';
import {useAppDispatch} from "../../hooks/redux";
import {resetWorkouts} from "../../store/reducers/WorkoutsSlice";
import "./style.scss"

const ThanksView = () => {
  const dispatch = useAppDispatch()

  return (
    <section className="section-thanks">
      <img src="/media/thanks.jpg" alt="Thank you"/>
      <h2>Thanks Mario,<br/>you are the best!</h2>
      <button onClick={()=>dispatch(resetWorkouts())}>Back to Home</button>
    </section>
  );
};

export default ThanksView;