import React, {useCallback, useEffect, useRef, useState} from 'react';


const FRAMES_COUNT = 20;
const FRAMES_TIMES = 2.5*100;


interface TmyHTMLVideoElement extends HTMLVideoElement {
  requestVideoFrameCallback: (computeFrame: (now: any, metadata: any) => void)=>void;
}

const NativeCamera = () => {

  const ourWorker:SharedWorker = new SharedWorker('./shared-worker.js');

  const canvasRef = useRef<HTMLCanvasElement>(null);
  const gifCanvasRef = useRef<HTMLCanvasElement>(null);
  const videoRef = useRef<TmyHTMLVideoElement>(null);


  const getStream =  ()=>{
    const constraints = { audio: false, video: true };
    navigator.mediaDevices.getUserMedia(constraints)
      .then(function(mediaStream) {
        const video = videoRef.current;
        if(video) {
          video.srcObject = mediaStream;
          video.onloadedmetadata = function (e) {
            video.play();
          };
          videoRef.current.requestVideoFrameCallback(computeFrame)
        }
      })
      .catch(function(err) { console.log(err.name + ": " + err.message); });
  }
  
  let startTime = 0.0;
  const computeFrame = (now: any, metadata: any)=>{
    if (canvasRef.current && videoRef.current) {

      const context = canvasRef.current.getContext('2d');
      if (context) {
        context.drawImage(videoRef.current, 0, 0, 320, 180);

        // Send the image to the worker once on FRAMES_TIMES in second.
        if(startTime + FRAMES_TIMES < now ){

          const imgData = context.getImageData(0, 0, 320, 180);
          ourWorker.port.postMessage({ type: 'imagedata', msg: imgData});

          startTime = now;
        }

      }

      videoRef.current.requestVideoFrameCallback(computeFrame)
    }

    /*const length = frame.data.length;
      for (let i = 0; i < length; i += 4) {
        const red = data[i + 0];
        const green = data[i + 1];
        const blue = data[i + 2];
        if (green > 100 && red > 100 && blue < 43) {
          data[i + 3] = 0;
        }
      }
      gifCanvasRef.current?.putImageData(frame, 0, 0);*/
  }

  const sendFlag = ()=>{
    ourWorker.port.postMessage({ type:'stopflag', msg:null });
  }


  const playGif = (data:ImageData[][], setIdx:number, frameIdx:number)=>{
    gifCanvasRef.current?.getContext('2d')?.putImageData(data[setIdx][frameIdx], 0, 0);
    frameIdx = frameIdx+1;
    if(frameIdx > data[setIdx].length-1){
      frameIdx = 0;
      setIdx = setIdx+1;
      if(setIdx > data.length-1){
        setIdx = 0;
      }
    }
    setTimeout(()=>playGif(data, setIdx, frameIdx), FRAMES_TIMES)
  }

  const getImages = ()=>{
    ourWorker.port.postMessage({ type:'getdata', msg:null });
  }


  const togglePlay = ()=>{
    const video = videoRef.current;
    if(video){
      video.paused ? video.play() : video.pause()
    }
  }

  useEffect(() => {

    ourWorker.port.addEventListener("message", (event: MessageEvent) => {
      const response = event.data;
      if(response.type === 'saveddata'){
        const data = response.msg;
        playGif(data, 0, 0);
      }
      console.log(response);
    }, false);

    ourWorker.port.start();

    /*ourWorker.onmessage = (event: MessageEvent) => {
      const response = event.data;
      if(response.type === 'saveddata'){
        const data = response.msg;
        data.forEach((set: ImageData[])=>{
          set.forEach((frame: ImageData)=>{
            setTimeout(()=>playGif(frame), FRAMES_TIMES);
          })
        })
      }
      console.log(response.msg);
    };*/
  }, [ourWorker]);

  useEffect(()=>{

    getStream()

  }, [])

  return (
    <div className="video-stream" style={{display:'flex',flexDirection:'column',alignItems:'center'}}>
      <video ref={videoRef} width={640} height={360} />
      <div>
        <canvas ref={canvasRef} width={320} height={180} />
      </div>
      <div>
        <canvas ref={gifCanvasRef} width={320} height={180} />
      </div>
      <div>
        <button onClick={togglePlay} style={{fontSize:'1.1rem',border:'1px white solid',borderRadius:'15px',height:'30px',padding:'5px 20px',margin:'0 10px'}}>Play / Pause</button>
        <button onClick={sendFlag} style={{fontSize:'1.1rem',border:'1px white solid',borderRadius:'15px',height:'30px',padding:'5px 20px',margin:'0 10px'}}>Save fragment</button>
        <button onClick={getImages} style={{fontSize:'1.1rem',border:'1px white solid',borderRadius:'15px',height:'30px',padding:'5px 20px',margin:'0 10px'}}>Show fragment</button>
      </div>
    </div>
  );
};

export default NativeCamera;