import React, { FC } from 'react';

interface Props {
  text: string;
}

const PageTitle:FC<Props> = ({text}) => {
  return (
    <h1>{text}</h1>
  );
};

export default PageTitle;