import React from 'react';
import "./style.scss";

const Footer = () => {
  return (
    <footer>
      <a href="https://app.kemtai.com/home">Already Using Kemtai</a>
    </footer>
  );
};

export default Footer;