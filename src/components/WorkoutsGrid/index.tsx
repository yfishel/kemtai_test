import React, { FC, memo } from 'react';
import {IWorkout} from "../../models/IWorkout";
import WorkoutCard from "../WorkoutCard";

interface Props {
  workouts: IWorkout[];
}

const WorkoutsGrid: FC<Props> = ({workouts}) => {
  return (
    <>
      {workouts.map((item) =>
        <WorkoutCard key={item.id} id={item.id} title={item.title} image={item.image} duration={ item.duration } />
      )}
    </>
  );
};

export default memo(WorkoutsGrid);