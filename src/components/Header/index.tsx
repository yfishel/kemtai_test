import React from 'react';
import "./style.scss";

const Header = () => {
  return (
    <header>
      <img src="/media/logo.png" className="header-logo" alt="Kemtai" />
    </header>
  );
};

export default Header;