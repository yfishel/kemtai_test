import React, { useEffect} from 'react';
import {FullScreen} from "@chiragrupani/fullscreen-react";
import {useAppDispatch, useAppSelector} from "./hooks/redux";
import { fetchWorkouts } from "./store/reducers/ActionCreators";
import Loader from "./components/Loader";
import MainView from "./components/MainView";
import CameraView from "./components/CameraView";
import NativeCamera from "./components/NativeCamera";

import './styles/App.scss';


function App() {
  const dispatch = useAppDispatch()
  const {workouts, selected, selectedReps, isLoading} = useAppSelector(state => state.workoutReducer)

  useEffect(()=>{
    dispatch(fetchWorkouts())
  }, [])
  
  return (
    <div className="App">

      {isLoading && <Loader />}

      <FullScreen onChange={()=>{}} isFullScreen={!!selectedReps} >



      {selected && selectedReps
        ?  <CameraView />
        :  <NativeCamera /> /*<MainView workouts={workouts} />*/
      }
      </FullScreen>

    </div>
  );
}

export default App;
