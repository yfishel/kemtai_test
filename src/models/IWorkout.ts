export interface IReps {
  TimeOfMove: [];
  ScoreOfMove: [];
}

export interface IWorkout {
  id: string;
  title: string;
  image: string;
  duration: number;
  reps: IReps
}
