import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IWorkout, IReps} from "../../models/IWorkout";
import {fetchWorkouts} from "./ActionCreators";


interface WorkoutsState {
  workouts: IWorkout[];
  selected: string | null;
  selectedReps: IReps | null;
  scorebar: number[];
  isLoading: boolean;
  error: string;
}

const initialState: WorkoutsState = {
  workouts: [],
  selected: null,
  selectedReps: null,
  scorebar: [],
  isLoading: false,
  error: ''
}

export const WorkoutsSlice = createSlice({
  name: 'workouts',
  initialState,
  reducers: {
    selectWorkout(state, action: PayloadAction<string>) {
      state.selected = action.payload;
      const selectedReps = state.workouts.find((workout) => workout.id===action.payload)?.reps;
      if(selectedReps){
        state.selectedReps = selectedReps;
        state.scorebar.length = selectedReps.ScoreOfMove.length;
        state.scorebar.fill(-1)
      }

    },
    resetWorkouts(state) {
      state.selected = null;
      state.selectedReps = null;
    },
    updateScoreBar(state, action: PayloadAction<number>) {
      state.scorebar[action.payload] = state.selectedReps?.ScoreOfMove[action.payload] || 0;
    }
  },
  extraReducers: {
    [fetchWorkouts.fulfilled.type]: (state, action: PayloadAction<IWorkout[]>) => {
      state.isLoading = false;
      state.error = '';
      state.workouts = action.payload;
    },
    [fetchWorkouts.pending.type]: (state) => {
      state.isLoading = true;
    },
    [fetchWorkouts.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
  }
})

export const { selectWorkout, resetWorkouts, updateScoreBar } = WorkoutsSlice.actions;
export default WorkoutsSlice.reducer