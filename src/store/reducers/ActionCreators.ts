import axios from "axios";
import {createAsyncThunk} from "@reduxjs/toolkit";
import { URLS } from "../../constants/";
import {IWorkout} from "../../models/IWorkout";

export const fetchWorkouts = createAsyncThunk(
  'workouts/fetchAll',
  async (_, thunkAPI) =>{
    try {
      const response = await axios.get<IWorkout[]>(URLS.WORKOUTS)
      return response.data
    } catch (e) {
      return thunkAPI.rejectWithValue('Error loading dummy data!')
    }
  }
)
