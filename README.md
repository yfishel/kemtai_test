# Kemtai test app #

Finish app - [https://yfishel.bitbucket.io](https://yfishel.bitbucket.io)

### "Dummy data" ###

"Dummy data" used in project - [https://api.json-generator.com/templates/5SRSRhYU5fXz/data?access_token=tczgl6968xt3v5tw1x7m1n1f6w4zjrc3rzspme3r](https://api.json-generator.com/templates/5SRSRhYU5fXz/data?access_token=tczgl6968xt3v5tw1x7m1n1f6w4zjrc3rzspme3r)

### Used NPM packages ###

* fontawesome
* fullscreen-react
* react-circular-progressbar
* react-webcam
